-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2020 at 01:12 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biru_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_approval`
--

CREATE TABLE `tbl_approval` (
  `id_approval` int(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `id_timesheet` int(20) NOT NULL,
  `date_approval` datetime NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_approval`
--

INSERT INTO `tbl_approval` (`id_approval`, `id_user`, `id_timesheet`, `date_approval`, `status`) VALUES
(1, 5, 1, '2020-07-01 11:00:00', 'PENDING'),
(2, 2, 1, '2020-07-08 15:00:00', 'PENDING');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cost_control`
--

CREATE TABLE `tbl_cost_control` (
  `id_cost_control` int(20) NOT NULL,
  `id_category` int(20) NOT NULL,
  `id_user` int(11) NOT NULL,
  `cost_control_code` varchar(30) NOT NULL,
  `cost_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cost_control`
--

INSERT INTO `tbl_cost_control` (`id_cost_control`, `id_category`, `id_user`, `cost_control_code`, `cost_description`) VALUES
(1, 1, 1, 'MEPI-CTR002', 'Specification Vacuum Truck and Tank Truck'),
(2, 1, 1, 'MEPI-CTR006', 'Integrated Schedule and Dashboard Development'),
(3, 2, 1, 'MEPI-CTR010', 'FEED Review for 3 Flowlines (infill) Kerang dan Semoga'),
(4, 1, 1, 'MEPI-CTR011', 'FEED Review for 3 Flowlines (infill) Kerang, Semoga, and Kaji'),
(5, 1, 2, 'MEPI-CTR012', 'Business Process Guideline'),
(6, 1, 2, 'MEPI-CTR013', 'As-Built South Sumatera Block'),
(7, 1, 2, 'MEPI-CTR014', 'SSB Flare Assessment'),
(8, 1, 1, 'MEPI-CTR017', 'RLA and Re-Engineering Gas Custody Meter'),
(9, 2, 1, 'MEPI-CTR019', 'Maintenance and Realibility Guideline'),
(10, 2, 1, 'MEPI-CTR020', 'Alarm Management Guideline'),
(11, 2, 2, 'MEPI-CTR021', 'As-Built Electrical South Suamtera Block'),
(12, 6, 2, 'MEPI-CTR022', 'Landslide Mitigation Analysis'),
(13, 6, 2, 'OPHIR-CTR025', 'OPF Grati Pressure Lowering DED Project'),
(14, 2, 1, 'OPHIR-CTR026', 'DED for Additional Air Receiver at OPF Grati'),
(15, 1, 1, 'OPHIR-CTR042', 'Modification for KGPF PWT Tank A'),
(16, 2, 2, 'OPHIR-CTR044', 'OPF Grati Admin Building Modification'),
(17, 1, 2, 'OPHIR-CTR049', 'Study Hydrate Prevention and Mitigation Kerendan Well'),
(18, 6, 1, 'OPHIR-CTR060', 'As-Built Campaign for Plant Modification'),
(19, 6, 2, 'OPHIR-CTR061', 'KC Well Flowline Replacement Assesstment, Kerandan Gas Field'),
(20, 2, 1, 'OPHIR-CTR062', 'Grati Green House, Grati, East Java '),
(21, 2, 2, 'OPHIR-CTR063', 'Bangkanai & Luwe Hulu Lightning  Protection Study'),
(22, 2, 2, 'OPHIR-CTR065', 'As-built P&ID KGPF Condensate Burner System'),
(23, 6, 1, 'OPHIR-CTR066', 'Bangkanai New Condensate Meter'),
(24, 6, 1, 'OPHIR-CTR067', 'Instrumentation Specialist Services '),
(25, 6, 2, 'OPHIR-CTR068', 'Mechanical Rotating Specialist Services'),
(26, 1, 1, 'OPHIR-CTR069', 'HAZOP Revalidation Sampang'),
(27, 1, 1, 'OPHIR-CTR071', 'Pipeline Hydraulic Assessment'),
(28, 2, 2, 'OPHIR-CTR072', 'Pigging Facility Design of Sales Gas Metering Area'),
(29, 2, 1, 'PHEWMO-CTR004', 'PHE-38B5 Temporary Surface Facility Assessment for ESP Installation'),
(30, 6, 1, 'PHEWMO-CTR005', 'PHE-40A5 Temporary Surface Facility Assessment for ESP Installation'),
(31, 2, 2, 'PHEWMO-CTR006', 'PHE-40R Permanent Surface Facility Assessment for ESP Installation'),
(32, 1, 2, 'PHEWMO-CTR007', 'PPP Flow Meter Monitoring System'),
(33, 2, 1, 'PHEWMO-CTR009', 'Flow Assurance Study for North Field'),
(34, 2, 1, 'PHEWMO-CTR013', 'Pipeline Protection Design'),
(35, 1, 1, 'PHEWMO-CTR016', 'Risers Analysis for PHE-12 Platform'),
(36, 2, 2, 'PHEWMO-CTR017', 'Supporting Study for PHE-38B and PHE-40A due to ESP Installation'),
(37, 1, 1, 'PHEWMO-CTR018', 'Detailed Engineering Services for  Fuel Gas System on PHE-38B'),
(38, 1, 2, 'PEP-CTR001C', 'Basis of Design, Philosophy and Specification'),
(39, 2, 1, 'PEP-CTR001D', 'Eng. Management and to Develop Balance of Deliverables'),
(40, 2, 2, 'PEP-CTR006EP', 'Pipeline Free Span Analysis'),
(41, 1, 2, 'PEP-CTR007EP', 'XAS Lifeboat Holder Replacement'),
(42, 1, 2, 'PEP-CTR010EP', 'Structural Analysis for BW Platform'),
(43, 2, 2, 'PEP-CTR016EP', 'Survey Metocean'),
(44, 1, 2, 'PEP-CTR017EP', 'Constructability Study and Cost Estimate for Additional \r\nCooling for AGRU and DHU CPP Gundih\"'),
(45, 1, 1, 'PEP-CTR018EP', 'Feseability Study Tahap 2 Crude Oil Custody Meter Terminal Sorong'),
(46, 2, 2, 'PEP-CTR021EP', 'Feasibility Study for H2S and CO2 Removal at Sukowati Gas Field, East Java'),
(47, 2, 2, 'PEP-CTR022EP', 'Assessment of XRray Platform due to impact of Seismic Exploration Around the Structure '),
(48, 1, 1, 'PEP-CTR023EP', 'In-Place Analysis and Boat Impact for New Boat Landing on TLA, TLD, and TLE Platform, Balongan West Java'),
(49, 2, 1, 'OTHERS-PROJECT', 'Others Project (Blanket)'),
(50, 1, 2, 'OTHERS-PROJSUP', 'Non Blanket Support - Tender, Marketing, Busdev'),
(51, 1, 2, 'INT-TRAINING', 'Internal PT Biru - Training'),
(52, 1, 2, 'INT-DIV', 'Internal PT Biru - Internal Divisi '),
(53, 1, 2, 'INT-OTHERS', 'Internal PT Biru - Non Internal Divisi'),
(54, 2, 2, 'ABS-CUTAH', 'Non Productive- Cuti Tahunan'),
(55, 2, 1, 'ABS-CULIB', 'Non Productive- Cuti Pengganti Libur'),
(56, 2, 1, 'ABS-CUSIT', 'Non Productive- Cuti Site Visit'),
(57, 6, 1, 'ABS-CUWA', 'Non Productive- Cuti Wajib Dibayar (melahirkan, pernikahan, dll)'),
(58, 6, 2, 'ABS-UNPAID', 'Non Productive- Cuti Tidak Dibayar/ Unpaid Leave'),
(59, 2, 2, 'ABS-SICK', 'Non Productive- Sick'),
(60, 1, 2, 'ABS-HOL', 'Non Productive- Vacation/Holiday'),
(61, 1, 1, 'ABS-IDDLE', 'Non Productive- Iddle');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_activity`
--

CREATE TABLE `tbl_m_activity` (
  `id_activity` int(30) NOT NULL,
  `activity_code` varchar(20) NOT NULL,
  `activity_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_m_activity`
--

INSERT INTO `tbl_m_activity` (`id_activity`, `activity_code`, `activity_name`) VALUES
(1, 'GEN', 'General'),
(2, 'MET', 'Meeting'),
(3, 'SVI', 'Site Visit'),
(4, 'SVR', 'Site Visit Report'),
(5, 'DBP', 'Design Basis/Philosophy'),
(6, 'SIM', 'Simulation/Study'),
(7, 'CAL', 'Calculation/Sizing'),
(8, 'SPE', 'Specification'),
(9, 'DS', 'Datasheet'),
(10, 'DRW', 'Drawing'),
(11, 'MTO', 'Material Take Off'),
(12, 'IDC', 'Inter Discipline Check'),
(13, 'SOD', 'Support Other Discipline'),
(14, 'CTR', 'CTR Preparation'),
(15, 'DRV', 'Driving'),
(16, 'ESC', 'Escort'),
(17, 'DRO', 'Drop Off'),
(18, 'PLA', 'Planning'),
(19, 'CON', 'Controlling'),
(20, 'EXP', 'Expediting'),
(21, 'COO', 'Coordinating'),
(22, 'COM', 'Communicating/ Liaise'),
(23, 'REP', 'Reporting'),
(24, 'DISC', 'Discussion'),
(25, 'MON', 'Monitoring'),
(26, 'CMN', 'Contract Monitoring'),
(29, 'CMN1', 'test'),
(31, 'tresd', 'sac');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_category`
--

CREATE TABLE `tbl_m_category` (
  `id_category` int(20) NOT NULL,
  `category_code` varchar(30) NOT NULL,
  `category_name` varchar(30) NOT NULL,
  `category_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_m_category`
--

INSERT INTO `tbl_m_category` (`id_category`, `category_code`, `category_name`, `category_description`) VALUES
(1, 'CTG0001', 'Project', 'Kategori project'),
(2, 'CTG0002', 'Non Project', 'Bukan project'),
(6, 'CTG0003', 'Non Produktif', 'Tidak produktif'),
(7, 'CTG0004', 'asc', 'asc');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_client`
--

CREATE TABLE `tbl_m_client` (
  `id_client` int(30) NOT NULL,
  `client_code` varchar(30) NOT NULL,
  `client_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_m_client`
--

INSERT INTO `tbl_m_client` (`id_client`, `client_code`, `client_name`) VALUES
(1, 'ALL', 'PEP All Asset'),
(2, 'EP1', 'PEP Asset 1'),
(3, 'EP2', 'PEP Asset 2'),
(4, 'EP3', 'PEP Asset 3'),
(5, 'EP4', 'PEP Asset 4'),
(6, 'MES', 'Medco Sampang Pty Ltd.'),
(7, 'OPM', 'OPHIR-MEDCO'),
(8, 'MEI', 'MEDCO-EP Indonesia'),
(9, 'MEN', 'MEDCO-EP Natuna'),
(10, 'WMO', 'PHE WMO'),
(12, 'Test', 'Modal');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_location`
--

CREATE TABLE `tbl_m_location` (
  `id_location` int(20) NOT NULL,
  `location_code` varchar(30) NOT NULL,
  `location_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_m_location`
--

INSERT INTO `tbl_m_location` (`id_location`, `location_code`, `location_name`) VALUES
(1, 'BiRU', 'BiRU'),
(2, 'TIP', 'TIP'),
(3, 'SITE', 'SITE'),
(4, 'CLI', 'Client\'s Office'),
(5, 'VEN', 'Vendor Office'),
(6, 'VEN', 'ascsac');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_user`
--

CREATE TABLE `tbl_m_user` (
  `id_user` int(20) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `user_nud` varchar(30) NOT NULL,
  `position` varchar(20) NOT NULL,
  `department` varchar(20) NOT NULL,
  `leader_name` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL,
  `status` int(1) NOT NULL,
  `report_to_pe` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_m_user`
--

INSERT INTO `tbl_m_user` (`id_user`, `user_name`, `user_nud`, `position`, `department`, `leader_name`, `email`, `password`, `role`, `status`, `report_to_pe`) VALUES
(1, 'Witchwicky', 'BIRU09111001', 'Atasan', 'Accountant', 'John Deep', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'SUPERADMIN', 1, 1),
(2, 'Marshall D Teach', 'BIRU111009221', 'Atasan', 'Comander', 'Edward Newgate', 'teach@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 'USER', 1, 0),
(5, 'Roronoa Zoro', 'BIRU312113131', 'Counter', 'Attacker', 'Mihawk', 'zoro@gmail.com', 'ee11cbb19052e40b07aac0ca060c23ee', 'USER', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timesheet`
--

CREATE TABLE `tbl_timesheet` (
  `id_timesheet` int(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `id_cost_control` int(20) NOT NULL,
  `id_client` int(20) NOT NULL,
  `id_location` int(20) NOT NULL,
  `id_activity` int(20) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `timesheet_information` varchar(255) NOT NULL,
  `date_sheet` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `status` enum('DRAFT','WAPPR','APPR PE','APRR','REJECT') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_timesheet`
--

INSERT INTO `tbl_timesheet` (`id_timesheet`, `id_user`, `id_cost_control`, `id_client`, `id_location`, `id_activity`, `attachment`, `timesheet_information`, `date_sheet`, `start_time`, `end_time`, `status`) VALUES
(1, 5, 2, 1, 2, 1, 'document.pdf', 'percobaan fitur timesheet', '2020-07-01', '10:00:00', '15:00:00', 'DRAFT'),
(2, 2, 1, 2, 1, 2, 'fgggggggg', 'test', '2020-07-01', '16:00:00', '21:00:00', 'WAPPR'),
(3, 2, 2, 1, 2, 1, 'thbgfb', 'tyn', '2020-07-03', '08:00:00', '15:00:00', 'APPR PE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_approval`
--
ALTER TABLE `tbl_approval`
  ADD PRIMARY KEY (`id_approval`);

--
-- Indexes for table `tbl_cost_control`
--
ALTER TABLE `tbl_cost_control`
  ADD PRIMARY KEY (`id_cost_control`);

--
-- Indexes for table `tbl_m_activity`
--
ALTER TABLE `tbl_m_activity`
  ADD PRIMARY KEY (`id_activity`);

--
-- Indexes for table `tbl_m_category`
--
ALTER TABLE `tbl_m_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `tbl_m_client`
--
ALTER TABLE `tbl_m_client`
  ADD PRIMARY KEY (`id_client`);

--
-- Indexes for table `tbl_m_location`
--
ALTER TABLE `tbl_m_location`
  ADD PRIMARY KEY (`id_location`);

--
-- Indexes for table `tbl_m_user`
--
ALTER TABLE `tbl_m_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tbl_timesheet`
--
ALTER TABLE `tbl_timesheet`
  ADD PRIMARY KEY (`id_timesheet`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_approval`
--
ALTER TABLE `tbl_approval`
  MODIFY `id_approval` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_cost_control`
--
ALTER TABLE `tbl_cost_control`
  MODIFY `id_cost_control` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `tbl_m_activity`
--
ALTER TABLE `tbl_m_activity`
  MODIFY `id_activity` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tbl_m_category`
--
ALTER TABLE `tbl_m_category`
  MODIFY `id_category` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_m_client`
--
ALTER TABLE `tbl_m_client`
  MODIFY `id_client` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_m_location`
--
ALTER TABLE `tbl_m_location`
  MODIFY `id_location` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_m_user`
--
ALTER TABLE `tbl_m_user`
  MODIFY `id_user` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_timesheet`
--
ALTER TABLE `tbl_timesheet`
  MODIFY `id_timesheet` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
