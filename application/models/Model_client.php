<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_client extends CI_Model {
	public function getClient($id = null){
        if(isset($id)){
            $this->db->where("id_client", $id);
        }
        $this->db->select("client.id_client, client.client_code, client.client_name");
        $this->db->from("tbl_m_client client");
        $this->db->order_by("id_client", "desc");
        $data = $this->db->get();
        if($data->num_rows() > 0){
            return $data->result();
        } else {
            return false;
        }
    }

    public function add_client($data){
        $this->db->insert("tbl_m_client", $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function edit_client($data, $id_client){
        $this->db->where("id_client", $id_client)->update("tbl_m_client",$data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function delete_client($idClient){
        $this->db->where("id_client", $idClient)->delete("tbl_m_client");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
}
