<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_timesheet extends CI_Model {
	public function getTimesheet($id = null){
        if(isset($id)){
            $this->db->where("id_timesheet", $id);
        }
        $this->db->select("tms.*, cst.id_category, cst.id_cost_control, cst.cost_control_code as codecost, cst.cost_description as descriptcost, clt.id_client, clt.client_code as codecli, lct.id_location, lct.location_code as codeloc, act.id_activity, act.activity_code as codeact, usr.user_name as usename");
		$this->db->from("tbl_timesheet tms");
        $this->db->join("tbl_cost_control cst", "cst.id_cost_control=tms.id_cost_control", "LEFT");
        $this->db->join("tbl_m_client clt", "clt.id_client=tms.id_client", "LEFT");
        // $this->db->join("tbl_m_category ctg", "ctg.id_category=tms.id_category", "LEFT");
        $this->db->join("tbl_m_location lct", "lct.id_location=tms.id_location", "LEFT");
        $this->db->join("tbl_m_activity act", "act.id_activity=tms.id_activity", "LEFT");
        $this->db->join("tbl_m_user usr", "usr.id_user=tms.id_user", "LEFT");
		$this->db->order_by("id_timesheet", "desc");
        $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }
}