<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_category extends CI_Model {
	public function getCategory($id = null){
        if(isset($id)){
            $this->db->where("id_category", $id);
        }
        $this->db->select("*");
        $this->db->from("tbl_m_category category");
        $this->db->order_by("id_category", "desc");
        $data = $this->db->get();
        if($data->num_rows() > 0){
            return $data->result();
        } else {
            return false;
        }
    }

    public function add_category($data){
        $this->db->insert("tbl_m_category", $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function edit_category($data, $id_category){
        $this->db->where("id_category", $id_category)->update("tbl_m_category",$data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function delete_category($idCategory){
        $this->db->where("id_category", $idCategory)->delete("tbl_m_category");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function check_existing_code($column, $value){
            $this->db->select("id_category");
            $this->db->from("tbl_m_category");
            $this->db->where($column, $value);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }
}