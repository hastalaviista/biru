<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_approve extends CI_Model {
	public function getApprove($id = null){
        if(isset($id)){
            $this->db->where("id_approve", $id);
        }
        $this->db->select("tms.id_timesheet, tms.id_user, tms.date_sheet, tms.id_cost_control, TIME_FORMAT(TIMEDIFF(tms.start_time, tms.end_time), '%k') as diffsheet, tms.status, cst.id_cost_control, cst.id_category, cst.id_user as leader, usr.user_name as nameuse");
		$this->db->from("tbl_timesheet tms");
        $this->db->join("tbl_cost_control cst", "cst.id_cost_control=tms.id_cost_control", "LEFT");
        $this->db->join("tbl_m_user usr", "usr.id_user=tms.id_user", "LEFT");
		$this->db->order_by("id_timesheet", "desc");
        $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }

    // public function getApprove($id = null){
    //     if(isset($id)){
    //         $this->db->where("id_approve", $id);
    //     }
    //     $this->db->select("apr.*, usr.user_name as usename, tms.date_sheet as datesheet, TIMEFORMAT(TIMEDIFF(tms.start_time, tms.end_time), '%k') as diffsheet, tms.id_cost_control, ");
    //     $this->db->from("tbl_approve apr");
    //     $this->db->join("tbl_timesheet tms", "tms.id_timesheet=apr.id_timesheet", "LEFT");
    //     $this->db->join("tbl_cost_control cst", "cst.id_cost_control=tms.id_cost_control", "LEFT");
    //     $this->db->join("tbl_m_user usr", "usr.id_user=tms.id_user", "LEFT");
    //     $this->db->order_by("id_approve", "desc");
    //     $data = $this->db->get();
    //         if($data->num_rows() > 0){
    //             return $data->result();
    //         } else {
    //             return false;
    //         }
    // }
}