<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model {

    public function getMasterOneTable($query, $table, $order, $sort, $condition){
        $this->db->select($query);
        $this->db->from($table);
        if ($condition != "") {
           $this->db->where($condition);
        }
        $this->db->order_by($order, $sort);
        return $this->db->get()->result();
    }

    public function tambahMaster($table, $data){
        $this->db->insert($table, $data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function editMaster($id_master, $table, $dataMaster){
        $this->db->where($id_master);
        $this->db->update($table, $dataMaster);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function get_status($query, $kolom, $keterangan, $order, $sort, $limit, $table){
        $this->db->select($query);
        $this->db->where(array($kolom => $keterangan )); 
        $this->db->order_by($order, $sort);
        $this->db->limit($limit);
        $proses = $this->db->get($table);
        
        if($proses->num_rows() > 0){
            return $proses->result();
        } else{
            return array();
        }
    }

    public function tambah_nilai($data){
        $this->db->insert_batch("tbl_nilai", $data);
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function editNilai($data){
        $this->db->insert_batch("tbl_nilai", $data);
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function check_tanggal($column){
            $this->db->select("id_periode");
            $this->db->from("tbl_periode");
            $this->db->where($column);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
        }

    public function view_user(){
        $this->db->select("COUNT(id_user) as jumlah");
        $this->db->from("tbl_m_user");
        $this->db->where("status", "1");
        // $this->db->order_by("id_cost_control");
        return $this->db->get()->row();
    }
    // SELECT COUNT(id_timesheet) as user FROM `tbl_timesheet` where status = "WAPPR"
    public function view_wappr(){
        $this->db->select("COUNT(id_timesheet) as jumlah");
        $this->db->from("tbl_timesheet");
        $this->db->where("status", "WAPPR");
        // $this->db->order_by("id_cost_control");
        return $this->db->get()->row();
    }

    public function view_apprpe(){
        $this->db->select("COUNT(id_timesheet) as jumlah");
        $this->db->from("tbl_timesheet");
        $this->db->where("status", "APPR PE");
        // $this->db->order_by("id_cost_control");
        return $this->db->get()->row();
    }

    public function view_reject(){
        $this->db->select("COUNT(id_timesheet) as jumlah");
        $this->db->from("tbl_timesheet");
        $this->db->where("status", "REJECT");
        // $this->db->order_by("id_cost_control");
        return $this->db->get()->row();
    }

  
}
?>