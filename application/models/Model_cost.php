<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cost extends CI_Model {
	public function getCost($id = null){
        if(isset($id)){
            $this->db->where("id_cost_control", $id);
        }
        $this->db->select("cst.*, usr.user_name as nameuse, ctg.category_name as namecot");
        $this->db->from("tbl_cost_control cst");
        $this->db->join("tbl_m_user usr", "usr.id_user=cst.id_user", "LEFT");
        $this->db->join("tbl_m_category ctg", "ctg.id_category=cst.id_category", "LEFT");
        $this->db->order_by("id_cost_control", "desc");
        $data = $this->db->get();
        if($data->num_rows() > 0){
            return $data->result();
        } else {
            return false;
        }
    }

    public function add_cost($data){
        $this->db->insert("tbl_cost_control", $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function edit_cost($data, $id_cost_control){
        $this->db->where("id_cost_control", $id_cost_control)->update("tbl_cost_control",$data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function delete_cost($idCost){
        $this->db->where("id_cost_control", $idCost)->delete("tbl_cost_control");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function category_list(){
        $this->db->select("id_category, category_name as list_name");
        $this->db->from("tbl_m_category");
        $this->db->order_by("list_name");
        return $this->db->get()->result();
    }

    public function user_list(){
        $this->db->select("id_user, user_name as list_name");
        $this->db->from("tbl_m_user");
        $this->db->where("position", "Atasan");
        $this->db->order_by("list_name");
        return $this->db->get()->result();
    }

    public function check_existing_code($column, $value){
            $this->db->select("id_cost_control");
            $this->db->from("tbl_cost_control");
            $this->db->where($column, $value);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
        }
}