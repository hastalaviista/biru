<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_activity extends CI_Model {
	public function getActivity($id = null){
        if(isset($id)){
            $this->db->where("id_activity", $id);
        }
        $this->db->select("activ.id_activity, activ.activity_code, activ.activity_name");
		$this->db->from("tbl_m_activity activ");
		$this->db->order_by("id_activity", "desc");
        $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }

    public function add_activity($data){
    	$this->db->insert("tbl_m_activity", $data);
    	if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function edit_activity($data, $id_activity){
    	$this->db->where("id_activity", $id_activity)->update("tbl_m_activity",$data);
    	if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function delete_activity($idActivity){
    	$this->db->where("id_activity", $idActivity)->delete("tbl_m_activity");
    	if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function check_existing_code($column, $value){
            $this->db->select("id_activity");
            $this->db->from("tbl_m_activity");
            $this->db->where($column, $value);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }

}