<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Periode extends CI_Model {

    public function getMasterOneTable($query, $table, $order, $sort, $condition){
        $this->db->select($query);
        $this->db->from($table);
        if ($condition != "") {
           $this->db->where($condition);
        }
        $this->db->order_by($order, $sort);
        return $this->db->get()->result();
    }

    public function tambahMaster($table, $data){
        $this->db->insert($table, $data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function editMaster($id_master, $table, $dataMaster){
        $this->db->where($id_master);
        $this->db->update($table, $dataMaster);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function get_status($query, $kolom, $keterangan, $order, $sort, $limit, $table){
        $this->db->select($query);
        $this->db->where(array($kolom => $keterangan )); 
        $this->db->order_by($order, $sort);
        $this->db->limit($limit);
        $proses = $this->db->get($table);
        
        if($proses->num_rows() > 0){
            return $proses->result();
        } else{
            return array();
        }
    }

    public function tambah_nilai($data){
        $this->db->insert_batch("tbl_nilai", $data);
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function editNilai($data){
        $this->db->insert_batch("tbl_nilai", $data);
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function check_tanggal($column){
            $this->db->select("date_start");
            $this->db->from("tbl_periode");
            $this->db->where("date_start", $column);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }

    public function get_periode($id = null){
        if(isset($id)){
            $this->db->where("id_periode", $id);
        }
        $this->db->select("periode.id_periode, periode.date_start, periode.date_end, periode.created, user.user_name as nm_user");
        $this->db->from("tbl_periode periode");
        $this->db->join("tbl_m_user user", "user.id_user = periode.id_user", "left");
        $this->db->order_by("id_periode", "desc");
        $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }

    public function view_cost_control($id_control){
        $this->db->select("control.id_cost_control, control.cost_control_code, control.cost_description, category.id_category, category.category_name");
        $this->db->from("tbl_cost_control control");
        $this->db->join("tbl_m_category category", "category.id_category = control.id_category", "left");
        $this->db->where("id_cost_control", $id_control);
        $this->db->order_by("id_cost_control");
        return $this->db->get()->result();
    }

    public function get_cost_control($id = null){
        if(isset($id)){
            $this->db->where("id_cost_control", $id);
        }
        $this->db->select("control.cost_control_code, control.cost_description, category.id_category, category.category_name");
        $this->db->from("tbl_cost_control control");
        $this->db->join("tbl_m_category category", "category.id_category = control.id_category", "left");
        $this->db->order_by("id_cost_control", "desc");
        $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }

    public function getTimesheet($id = null){
        if(isset($id)){
            $this->db->where("id_timesheet", $id);
        }
        $this->db->select("tms.*, ");
        $this->db->from("tbl_timesheet tms");
        $this->db->join("tbl_cost_control cst", "cst.id_cost_control=tms.id_cost_control", "LEFT");
        $this->db->join("tbl_m_client clt", "clt.id_client=tms.id_client", "LEFT");
        $this->db->join("tbl_m_location lct", "lct.id_location=tms.id_location", "LEFT");
        $this->db->join("tbl_m_act act", "act.id_activity=tms.id_activity", "LEFT");
        $this->db->join("tbl_m_user usr", "usr.id_user=tms.id_user", "LEFT");
        $this->db->order_by("id_timesheet", "desc");
        $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
    }

  
}
?>