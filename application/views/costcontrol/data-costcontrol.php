<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-user"></i>Master Data Cost Control</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Cost Control</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role') == 'SUPERADMIN'){ ?>
                    <!-- <button class="btn btn-outline btn-info dim" id="btn_add_data">
                        <i class="fa fa-plus"></i> Add Cost
                    </button> -->
                    <button id="btn_add_data" class="btn btn-info " type="button"><i class="fa fa-plus"></i> Add Cost</button>
                </div> <?php } ?>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-1 pull-right" style="margin-right:40px;">
                            <label><br/></label>
                                <a href="<?php echo base_url('pertanyaan/cetakPdf') ?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Print Data</a>
                        </div>
                        <div class="col-lg-1 pull-right" style="margin-right:10px;">
                            <label><br/></label>
                                <a href="<?php echo base_url('pertanyaan/export') ?>" data-toggle="tooltip" title="Download" class="btn btn-sm btn-primary"><i class="fa fa-file-excel-o"></i> Export</a>
                        </div>
                        
                    </div>
                    <div class="table-responsive">
                    <table id="tableCost" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Category</th>
                        <th>Cost Code</th>
                        <th>Cost Description</th>
                        <th>PE</th>
                        <?php if($this->session->userdata('role') == 'SUPERADMIN'){?>
                        <th width="15%">Action</th>
                        <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-cost">Add Cost</h6>
            </div>
            <div class="modal-body">
                <form id="formAddcost" class="form-horizontal">
                    <!-- <div class="form-group">
                        <label class="col-sm-3 control-label">Kategori</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="id_category" id="id_category" required>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="id_category" name="id_category" required>
                                    <option value="">Choose Category</option>
                                    <?php
                                        foreach ($listcategori as $l) {
                                            echo '<option value="'.$l->id_category.'">'.$l->list_name.'</option>';
                                        }
                                    ?>
                                </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cost Code</label>
                        <div class="col-sm-7">
                            <input type="hidden" class="form-control" name="id_cost_control" id="id_cost_control" required>
                            <input type="text" class="form-control" name="cost_control_code" id="cost_control_code" placeholder="Kode Cost Controll" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cost Description</label>
                        <div class="col-sm-7">
                            <textarea class="form-control" name="cost_description" id="cost_description" rows="4" required=""></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">PE</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="id_user" name="id_user" required>
                                    <option value="">Choose Leader</option>
                                    <?php
                                        foreach ($listuser as $l) {
                                            echo '<option value="'.$l->id_user.'">'.$l->list_name.'</option>';
                                        }
                                    ?>
                                </select>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center;">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>;
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("document").ready(function(){
        var tableCost = $("#tableCost").dataTable({
            processing: true,
            select: false,
            ajax: {
                url : "<?= base_url('costcontrol/get_data_cc') ?>",
                dataType : "JSON",
                type : "GET",
                dataSrc : function (data){
                    var returnDataclient = new Array();
                    if(data.status == "success"){
                        $.each(data["data"], function(i, item){
                            returnDataclient.push({
                                "no" : (i+1),
                                "namecot" : item["namecot"],
                                "cost_control_code" : item["cost_control_code"],
                                "cost_description" : item["cost_description"],
                                "nameuse" : item["nameuse"],
                                "action" : "<center><button id='btn_edit' data-id_cost_control='"+item["id_cost_control"]+"' class='btn btn-xs btn-warning'><i class='fa fa-edit'></i> Edit</button>&nbsp;"+
                                            "<button id='btn_delete' data-id_cost_control='"+item["id_cost_control"]+"' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</button></center>"
                            });
                        });
                    }
                    return returnDataclient;
                }
            },
            columns : [
                {data : "no"},
                {data : "namecot"},
                {data : "cost_control_code"},
                {data : "cost_description"},
                {data : "nameuse"},
                {data : "action"}
            ]
        });
    });

    $(document).on("click", "#btn_add_data", function(e){
        $('#formAddcost')[0].reset();
        $("#id_cost_control").val("");
        $("#modalAdd").modal("show");
    });

    $(document).on("click", "#btn_edit", function(e){
        e.preventDefault();
        $('#formAddcost')[0].reset();
        var id_cost_control = $(this).data("id_cost_control");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('costcontrol/get_data_cc') ?>/"+id_cost_control,
            "method": "GET",
        }).done(function (response) {
            var data = JSON.parse(response);
            $("#id_cost_control").val(data.data[0].id_cost_control);
            $("#id_category").val(data.data[0].id_category).change();
            $("#cost_control_code").val(data.data[0].cost_control_code);
            $("#cost_description").val(data.data[0].cost_description);
            $("#id_user").val(data.data[0].id_user).change();
            $("#modalAdd").modal("show");
        });
    });
    

    $(document).on("submit", "#formAddcost", function(e){
        e.preventDefault();
        var id_cost_control = $("#id_cost_control").val();
        var url = (id_cost_control == "" ? "addCost" : "editCost");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('costcontrol') ?>/"+url,
            "method": "POST",
            "data": $(this).serialize(),
        }).done(function (response) {
            var data = JSON.parse(response)
            var message = data.message;
            if(data.status == "success"){
                $("#modalAdd").modal("hide");
                swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#tableCost").DataTable().ajax.reload();
                });
                
            } else {
                swal("Gagal menambahkan.", message.toUpperCase(), "warning");
            }
        });
    });

    $(document).on("click", "#btn_delete", function(e){
        var id_cost_control = $(this).data("id_cost_control");
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('costcontrol/deleteCost') ?>",
                    "method": "POST",
                    "data": {
                        "id_cost_control": id_cost_control
                    }
                }
                
                $.ajax(settings).done(function (response) {
                    var data = JSON.parse(response)
                    var message = data.message;
                    if(data.status == "success"){
                        swal({
                            title: "Berhasil",
                            text: message.toUpperCase(),
                            type: "success",
                            confirmButtonColor: "#a5dc86",
                            confirmButtonText: "Close",
                        }, function(isConfirm){
                            $("#tableCost").DataTable().ajax.reload();
                        });
                    } else {
                        swal("Gagal menghapus data.", message.toUpperCase(), "warning");
                    }
                });   
            }
        });
    });
</script>