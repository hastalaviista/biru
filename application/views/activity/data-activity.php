<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-pencil-square-o"></i> Master Data Activity</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Activity</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role') == 'SUPERADMIN'){ ?>
                    <!-- <button class="btn btn-outline btn-info dim" id="btn_add_data">
                        <i class="fa fa-plus"></i> Add Activity
                    </button> -->
                    <button id="btn_add_data" class="btn btn-info " type="button"><i class="fa fa-plus"></i> Add Activity</button>
                </div> <?php } ?>
                <div class="ibox-content">
                    <div class="table-responsive">
                    <table id="tableActivity" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Activity Code</th>
                        <th>Activity</th>
                        <?php if($this->session->userdata('role') == 'SUPERADMIN'){?>
                        <th width="15%">Action</th>
                        <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-quis">Add Activity</h6>
            </div>
            <div class="modal-body">
                <form id="formAddactivity" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Activity Code</label>
                        <div class="col-sm-7">
                            <input type="hidden" class="form-control" name="id_activity" id="id_activity" required>
                            <input type="text" class="form-control" name="activity_code" id="activity_code" placeholder="Activity Code" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Activity</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="activity_name" id="activity_name" placeholder="Activity" required>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center;">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>;
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("document").ready(function(){
        var tableActivity = $("#tableActivity").dataTable({
            processing: true,
            select: false,
            ajax: {
                url : "<?= base_url('activity/get_data_activity') ?>",
                dataType : "JSON",
                type : "GET",
                dataSrc : function (data){
                    var returnDataactivity = new Array();
                    if(data.status == "success"){
                        $.each(data["data"], function(i, item){
                            returnDataactivity.push({
                                "no" : (i+1),
                                "activity_code" : item["activity_code"],
                                "activity_name" : item["activity_name"],
                                "action" : "<center><button id='btn_edit' data-id_activity='"+item["id_activity"]+"' class='btn btn-xs btn-warning'><i class='fa fa-edit'></i> Edit</button>&nbsp;"+
                                            "<button id='btn_delete' data-id_activity='"+item["id_activity"]+"' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</button></center>"
                            });
                        });
                    }
                    return returnDataactivity;
                }
            },
            columns : [
                {data : "no"},
                {data : "activity_code"},
                {data : "activity_name"},
                {data : "action"}
            ]
        });
    });

    $(document).on("click", "#btn_add_data", function(e){
        $('#formAddactivity')[0].reset();
        $("#id_activity").val("");
        $("#modalAdd").modal("show");
    });

    $(document).on("click", "#btn_edit", function(e){
        e.preventDefault();
        $('#formAddactivity')[0].reset();
        var id_activity = $(this).data("id_activity");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('activity/get_data_activity') ?>/"+id_activity,
            "method": "GET",
        }).done(function (response) {
            var data = JSON.parse(response);
            $("#id_activity").val(data.data[0].id_activity);
            $("#activity_code").val(data.data[0].activity_code);
            $("#activity_name").val(data.data[0].activity_name);
            $("#modalAdd").modal("show");
        });
    });
    

    $(document).on("submit", "#formAddactivity", function(e){
        e.preventDefault();
        var id_activity = $("#id_activity").val();
        var url = (id_activity == "" ? "addActivity" : "editActivity");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('activity') ?>/"+url,
            "method": "POST",
            "data": $(this).serialize(),
        }).done(function (response) {
            var data = JSON.parse(response)
            var message = data.message;
            if(data.status == "success"){
                $("#modalAdd").modal("hide");
                swal({
                    title: "Saving Data Success",
                    text: message,
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#tableActivity").DataTable().ajax.reload();
                });
                
            } else {
                swal("Saving Data Failed", message, "error");
            }
        });
    });

    $(document).on("click", "#btn_delete", function(e){
        var id_activity = $(this).data("id_activity");
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('activity/deleteActivity') ?>",
                    "method": "POST",
                    "data": {
                        "id_activity": id_activity
                    }
                }
                
                $.ajax(settings).done(function (response) {
                    var data = JSON.parse(response)
                    var message = data.message;
                    if(data.status == "success"){
                        swal({
                            title: "Berhasil",
                            text: message.toUpperCase(),
                            type: "success",
                            confirmButtonColor: "#a5dc86",
                            confirmButtonText: "Close",
                        }, function(isConfirm){
                            $("#tableActivity").DataTable().ajax.reload();
                        });
                    } else {
                        swal("Gagal menghapus data.", message.toUpperCase(), "warning");
                    }
                });   
            }
        });
    });
</script>