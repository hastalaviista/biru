<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-cubes"></i> Master Kategori</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Kategori</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role') == 'SUPERADMIN'){ ?>
                    <!-- <button class="btn btn-outline btn-info dim" id="btn_add_data">
                        <i class="fa fa-plus"></i> Add Kategori
                    </button> -->
                    <button id="btn_add_data" class="btn btn-info " type="button"><i class="fa fa-plus"></i> Add Kategori</button>
                </div> <?php } ?>
                <div class="ibox-content">
                    <div class="table-responsive">
                    <table id="tableCategory" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Kode Kategori</th>
                        <th>Kategori</th>
                        <th>Deskripsi</th>
                        <?php if($this->session->userdata('role') == 'SUPERADMIN'){?>
                        <th width="15%">Aksi</th>
                        <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-quis">Tambah Kategori</h6>
            </div>
            <div class="modal-body">
                <form id="formAddcategory" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kode Kategori</label>
                        <div class="col-sm-7">
                            <input type="hidden" class="form-control" name="id_category" id="id_category" required>
                            <input type="text" class="form-control" name="category_code" id="category_code" placeholder="Kode Kategori" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kategori</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Kategori" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Deskripsi Kategori</label>
                        <div class="col-sm-7">
                            <textarea class="form-control" name="category_description" id="category_description" rows="4" required=""></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center;">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>;
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("document").ready(function(){
        var tableCategory = $("#tableCategory").dataTable({
            processing: true,
            select: false,
            ajax: {
                url : "<?= base_url('category/get_data_category') ?>",
                dataType : "JSON",
                type : "GET",
                dataSrc : function (data){
                    var returnDatacategory = new Array();
                    if(data.status == "success"){
                        $.each(data["data"], function(i, item){
                            returnDatacategory.push({
                                "no" : (i+1),
                                "category_code" : item["category_code"],
                                "category_name" : item["category_name"],
                                "category_description" : item["category_description"],
                                "action" : "<center><button id='btn_edit' data-id_category='"+item["id_category"]+"' class='btn btn-xs btn-warning'><i class='fa fa-edit'></i> Edit</button>&nbsp;"+
                                            "<button id='btn_delete' data-id_category='"+item["id_category"]+"' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</button></center>"
                            });
                        });
                    }
                    return returnDatacategory;
                }
            },
            columns : [
                {data : "no"},
                {data : "category_code"},
                {data : "category_name"},
                {data : "category_description"},
                {data : "action"}
            ]
        });
    });

    $(document).on("click", "#btn_add_data", function(e){
        $('#formAddcategory')[0].reset();
        $("#id_category").val("");
        $("#modalAdd").modal("show");
    });

    $(document).on("click", "#btn_edit", function(e){
        e.preventDefault();
        $('#formAddcategory')[0].reset();
        var id_category = $(this).data("id_category");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('category/get_data_category') ?>/"+id_category,
            "method": "GET",
        }).done(function (response) {
            var data = JSON.parse(response);
            $("#id_category").val(data.data[0].id_category);
            $("#category_code").val(data.data[0].category_code);
            $("#category_name").val(data.data[0].category_name);
            $("#category_description").val(data.data[0].category_description);
            $("#modalAdd").modal("show");
        });
    });
    

    $(document).on("submit", "#formAddcategory", function(e){
        e.preventDefault();
        var id_category = $("#id_category").val();
        var url = (id_category == "" ? "addCategory" : "editCategory");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('category') ?>/"+url,
            "method": "POST",
            "data": $(this).serialize(),
        }).done(function (response) {
            var data = JSON.parse(response)
            var message = data.message;
            if(data.status == "success"){
                $("#modalAdd").modal("hide");
                swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#tableCategory").DataTable().ajax.reload();
                });
                
            } else {
                swal("Gagal menambahkan.", message.toUpperCase(), "warning");
            }
        });
    });

    $(document).on("click", "#btn_delete", function(e){
        var id_category = $(this).data("id_category");
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('category/deleteCategory') ?>",
                    "method": "POST",
                    "data": {
                        "id_category": id_category
                    }
                }
                
                $.ajax(settings).done(function (response) {
                    var data = JSON.parse(response)
                    var message = data.message;
                    if(data.status == "success"){
                        swal({
                            title: "Berhasil",
                            text: message.toUpperCase(),
                            type: "success",
                            confirmButtonColor: "#a5dc86",
                            confirmButtonText: "Close",
                        }, function(isConfirm){
                            $("#tableCategory").DataTable().ajax.reload();
                        });
                    } else {
                        swal("Gagal menghapus data.", message.toUpperCase(), "warning");
                    }
                });   
            }
        });
    });
</script>