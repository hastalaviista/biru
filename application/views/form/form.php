<html>
<head>
	<title>Form Import</title>

	<!-- Load File jquery.min.js yang ada difolder js -->
	<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

	<script>
	$(document).ready(function(){
		// Sembunyikan alert validasi kosong
		$("#kosong").hide();
	});
	</script>
</head>
<body>
	<!-- <h3>Form Import</h3> -->
	<hr>

	<!-- <a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a> -->
	<br>
	<br>

	<!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
	<form method="post" action="<?php echo base_url("Promex/form"); ?>" enctype="multipart/form-data">
		<!--
		-- Buat sebuah input type file
		-- class pull-left berfungsi agar file input berada di sebelah kiri
		-->
		<input type="file" name="file">

		<!--
		-- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
		-->
		<input type="submit" name="preview" value="Preview">
	</form>

<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}

		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("Promex/import")."'>";

		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";

		echo "<table border='1' cellpadding='8'>
		<tr>
			<th colspan='5'>Preview Data</th>
		</tr>
		<tr>
			<th>nama_akun</th>
			<th>nama_promo</th>
			<th>tgl_durasi_start</th>
			<th>tgl_durasi_end</th>
			<th>tgl_sell_out_total</th>
			<th>sku_total</th>
			<th>quantity_total</th>
			<th>price_total</th>
			<th>tgl_sell_out_onpromo</th>
			<th>sku</th>
			<th>quantity</th>
			<th>price</th>
			<th>biaya_fixed</th>
			<th>biaya_variable</th>
			<th>gp</th>
			<th>target_budget_tahunan</th>
			<th>budget_terpakai</th>
			<th>total_budget_terpakai</th>
			<th>margin</th>
		</tr>";

		$numrow = 1;
		$kosong = 0;

		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($sheet as $row){
			// Ambil data pada excel sesuai Kolom
			$nama_akun = $row['A']; // Ambil data NIS
			$nama_promo = $row['B']; // Ambil data nama
			$tgl_durasi_start = $row['C']; // Ambil data jenis kelamin
			$tgl_durasi_end = $row['D']; // Ambil data alamat
			$tgl_sell_out_tku_total = $row['E'];
			$sku_total = $row['F'];
			$quantity_total = $row['G'];
			$price_total = $row['H'];
			$tgl_sell_out_onpromo = $row['I'];
			$sku = $row['J'];
			$quantity = $row['K'];
			$price = $row['L'];
			$biaya_fixed = $row['M'];
			$biaya_variable = $row['N'];
			$gp = $row['O'];
			$target_budget_tahunan = $row['P'];
			$budget_terpakai = $row['Q'];
			$total_budget_terpakai = $row['R'];
			$margin = $row['S'];

			// Cek jika semua data tidak diisi
			if($nama_akun == "" && $nama_promo == "" && $tgl_durasi_start == "" && $tgl_durasi_end == "" && $tgl_sell_out_tku_total == ""
			 && $sku_total == "" && $quantity_total == "" && $price_total == ""
			  && $tgl_sell_out_onpromo == "" && $sku == "" && $quantity == "" && $price == "" && $biaya_fixed == "" && $biaya_variable == "" &&
				 $gp == "" && $target_budget_tahunan == "" && $budget_terpakai == "" && $total_budget_terpakai == "" && $margin == "")
				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Validasi apakah semua data telah diisi
				$nama_akun_td = ( ! empty($nama_akun))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
				$nama_promo_td = ( ! empty($nama_promo))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
				$tgl_durasi_start_td = ( ! empty($tgl_durasi_start))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
				$tgl_durasi_end_td = ( ! empty($tgl_durasi_end))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tgl_sell_out_tku_total_td = ( ! empty($tgl_sell_out_tku_total))? "" : " style='background: #E07171;'"; 
				$sku_total_td = ( ! empty($sku_total))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$quantity_total_td = ( ! empty($quantity_total))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$price_total_td = ( ! empty($price_total))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tgl_sell_out_onpromo_td = ( ! empty($tgl_sell_out_onpromo))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$sku_td = ( ! empty($sku))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$quantity_td = ( ! empty($quantity))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$price_td = ( ! empty($price))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$biaya_fixed_td = ( ! empty($biaya_fixed))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$biaya_variable_td = ( ! empty($biaya_variable))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$gp_td = ( ! empty($gp))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$target_budget_tahunan_td = ( ! empty($target_budget_tahunan))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$budget_terpakai_td = ( ! empty($budget_terpakai))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$total_budget_terpakai_td = ( ! empty($total_budget_terpakai))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$margin_td = ( ! empty($margin))? "" : " style='background: #E07171;'";
				// Jika salah satu data ada yang kosong
				if($nama_akun == "" or $nama_promo == "" or
					 $tgl_durasi_start == "" or $tgl_durasi_end == ""or $tgl_sell_out_tku_total == ""or $sku_total == ""or $quantity_total == ""or $price_total == ""or $tgl_sell_out_onpromo == ""or $sku == ""or $quantity == ""or $price == ""or $biaya_fixed == ""or $biaya_variable == ""or $gp == ""or $target_budget_tahunan == ""or $budget_terpakai == ""or $total_budget_terpakai == ""or $margin == ""){
					$kosong++; // Tambah 1 variabel $kosong
				}

				echo "<tr>";
				echo "<td".$nama_akun_td.">".$nama_akun."</td>";
				echo "<td".$nama_promo_td.">".$nama_promo."</td>";
				echo "<td".$tgl_durasi_start_td.">".$tgl_durasi_start."</td>";
				echo "<td".$tgl_durasi_end_td.">".$tgl_durasi_end."</td>";
				echo "<td".$tgl_sell_out_tku_total_td.">".$tgl_sell_out_tku_total."</td>";
				echo "<td".$sku_total_td.">".$sku_total."</td>";
				echo "<td".$quantity_total_td.">".$quantity_total."</td>";
				echo "<td".$price_total_td.">".$price_total."</td>";
				echo "<td".$tgl_sell_out_onpromo_td.">".$tgl_sell_out_onpromo."</td>";
				echo "<td".$sku_td.">".$sku."</td>";
				echo "<td".$quantity_td.">".$quantity."</td>";
				echo "<td".$price_td.">".$price."</td>";
				echo "<td".$biaya_fixed_td.">".$biaya_fixed."</td>";
				echo "<td".$biaya_variable_td.">".$biaya_variable."</td>";
				echo "<td".$gp_td.">".$gp."</td>";
				echo "<td".$target_budget_tahunan_td.">".$target_budget_tahunan."</td>";
				echo "<td".$budget_terpakai_td.">".$budget_terpakai."</td>";
				echo "<td".$total_budget_terpakai_td.">".$total_budget_terpakai."</td>";
				echo "<td".$margin_td.">".$margin."</td>";
				echo "</tr>";
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		echo "</table>";

		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
		if($kosong > 0){
		?>
			<script>
			$(document).ready(function(){
				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
				$("#jumlah_kosong").html('<?php echo $kosong; ?>');

				$("#kosong").show(); // Munculkan alert validasi kosong
			});
			</script>
		<?php
		}else{ // Jika semua data sudah diisi
			echo "<hr>";

			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button type='submit' name='import'>Import</button>";
			echo "<a href='".base_url('promo')."'>Cancel</a>";
		}

		echo "</form>";
	}
	?> 
</body>
</html>
