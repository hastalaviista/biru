<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-user"></i> Master Data Client</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Client</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role') == 'SUPERADMIN'){ ?>
                    <!-- <button class="btn btn-outline btn-info dim" id="btn_add_data">
                        <i class="fa fa-plus"></i> Add Client
                    </button> -->
                    <button id="btn_add_data" class="btn btn-info " type="button"><i class="fa fa-plus"></i> Add Klien</button>
                </div> <?php } ?>
                <div class="ibox-content">
                    <div class="table-responsive">
                    <table id="tableClient" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Client Code</th>
                        <th>Client Name</th>
                        <?php if($this->session->userdata('role') == 'SUPERADMIN'){?>
                        <th width="15%">Action</th>
                        <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-quis">Add Client</h6>
            </div>
            <div class="modal-body">
                <form id="formAddclient" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Client Code</label>
                        <div class="col-sm-7">
                            <input type="hidden" class="form-control" name="id_client" id="id_client" required>
                            <input type="text" class="form-control" name="client_code" id="client_code" placeholder="Client Code" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Client Name</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="client_name" id="client_name" placeholder="Client Name" required>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center;">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>;
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("document").ready(function(){
        var tableClient = $("#tableClient").dataTable({
            processing: true,
            select: false,
            ajax: {
                url : "<?= base_url('client/get_data_client') ?>",
                dataType : "JSON",
                type : "GET",
                dataSrc : function (data){
                    var returnDataclient = new Array();
                    if(data.status == "success"){
                        $.each(data["data"], function(i, item){
                            returnDataclient.push({
                                "no" : (i+1),
                                "client_code" : item["client_code"],
                                "client_name" : item["client_name"],
                                "action" : "<center><button id='btn_edit' data-id_client='"+item["id_client"]+"' class='btn btn-xs btn-warning'><i class='fa fa-edit'></i> Edit</button>&nbsp;"+
                                            "<button id='btn_delete' data-id_client='"+item["id_client"]+"' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</button></center>"
                            });
                        });
                    }
                    return returnDataclient;
                }
            },
            columns : [
                {data : "no"},
                {data : "client_code"},
                {data : "client_name"},
                {data : "action"}
            ]
        });
    });

    $(document).on("click", "#btn_add_data", function(e){
        $('#formAddclient')[0].reset();
        $("#id_client").val("");
        $("#modalAdd").modal("show");
    });

    $(document).on("click", "#btn_edit", function(e){
        e.preventDefault();
        $('#formAddclient')[0].reset();
        var id_client = $(this).data("id_client");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('client/get_data_client') ?>/"+id_client,
            "method": "GET",
        }).done(function (response) {
            var data = JSON.parse(response);
            $("#id_client").val(data.data[0].id_client);
            $("#client_code").val(data.data[0].client_code);
            $("#client_name").val(data.data[0].client_name);
            $("#modalAdd").modal("show");
        });
    });
    

    $(document).on("submit", "#formAddclient", function(e){
        e.preventDefault();
        var id_client = $("#id_client").val();
        var url = (id_client == "" ? "addClient" : "editClient");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('client') ?>/"+url,
            "method": "POST",
            "data": $(this).serialize(),
        }).done(function (response) {
            var data = JSON.parse(response)
            var message = data.message;
            if(data.status == "success"){
                $("#modalAdd").modal("hide");
                swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#tableClient").DataTable().ajax.reload();
                });
                
            } else {
                swal("Gagal menambahkan.", message.toUpperCase(), "warning");
            }
        });
    });

    $(document).on("click", "#btn_delete", function(e){
        var id_client = $(this).data("id_client");
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('client/deleteClient') ?>",
                    "method": "POST",
                    "data": {
                        "id_client": id_client
                    }
                }
                
                $.ajax(settings).done(function (response) {
                    var data = JSON.parse(response)
                    var message = data.message;
                    if(data.status == "success"){
                        swal({
                            title: "Berhasil",
                            text: message.toUpperCase(),
                            type: "success",
                            confirmButtonColor: "#a5dc86",
                            confirmButtonText: "Close",
                        }, function(isConfirm){
                            $("#tableClient").DataTable().ajax.reload();
                        });
                    } else {
                        swal("Gagal menghapus data.", message.toUpperCase(), "warning");
                    }
                });   
            }
        });
    });
</script>