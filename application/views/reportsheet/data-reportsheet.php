<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-users"></i> Master Data Pertanyaan</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Pertanyaan</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role') == 'SUPERADMIN'){ ?>
                    <div class="col-lg-1 pull-right" style="margin-right:10px;">
                            <label><br/></label>
                                <a href="<?php echo base_url('reportsheet/export') ?>" data-toggle="tooltip" title="Download" class="btn btn-sm btn-primary"><i class="fa fa-file-excel-o"></i> Ekspor</a>
                        </div>
                </div> <?php } ?>
                <div class="ibox-content">
                  
                  </div>
                </div>
            </div>
        </div>
    </div>