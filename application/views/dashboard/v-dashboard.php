<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-users"></i> Master Dashboard</h2>
        <ol class="breadcrumb">
            <!-- <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li> -->
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
      <!-- <div class="widget red-bg p-lg text-center">
          <div class="m-b-md">
              <i class="fa fa-bell fa-4x"></i>
              <h1 class="m-xs">47</h1>
              <h3 class="font-bold no-margins">
                  Notification
              </h3>
              <small>We detect the error.</small>
          </div>
      </div> -->
      <div class="col-lg-3">
          <div class="widget style1 navy-bg">
              <div class="row">
                  <div class="col-xs-4">
                      <i class="fa fa-user fa-5x"></i>
                  </div>
                  <div class="col-xs-8 text-right">
                      <span> Jumlah User Aktif </span>
                      <h2 class="font-bold"><?php echo $user->jumlah; ?></h2>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3">
          <div class="widget style1 lazur-bg">
              <div class="row">
                  <div class="col-xs-4">
                      <i class="fa fa-calendar fa-5x"></i>
                  </div>
                  <div class="col-xs-8 text-right">
                      <span> Submit Timesheets </span>
                      <h2 class="font-bold"><?php echo $apprpe->jumlah; ?></h2>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3">
          <div class="widget style1 yellow-bg">
              <div class="row">
                  <div class="col-xs-4">
                      <i class="fa fa-bell fa-5x"></i>
                  </div>
                  <div class="col-xs-8 text-right">
                      <span> Timesheets WAPPR </span>
                      <h2 class="font-bold"><?php echo $wappr->jumlah; ?></h2>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3">
          <div class="widget style1 red-bg">
              <div class="row">
                  <div class="col-xs-4">
                      <i class="fa fa-times fa-5x"></i>
                  </div>
                  <div class="col-xs-8 text-right">
                      <span> Timesheets Reject </span>
                      <h2 class="font-bold"><?php echo $reject->jumlah; ?></h2>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<script type="text/javascript">
Highcharts.chart('container', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Data Responden MAN Kediri 1'
  },
  subtitle: {
    text: 'Berdasarkan Kategori'
  },
  xAxis: {
    // categories: [
    //   'Jan',
    //   'Feb',
    //   'Mar',
    //   'Apr',
    //   'May',
    //   'Jun',
    //   'Jul',
    //   'Aug',
    //   'Sep',
    //   'Oct',
    //   'Nov',
    //   'Dec'
    // ],
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Total (Nilai)'
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.1f} votes</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [
    <?php
      foreach ($chartper as $charts) {
          echo "{";
          echo "name:'".$charts["name"]."',";
          echo "data:[".$charts["data"]."]";

          echo "},";
      }
    ?>
    // {
    //   name: 'SB',
    //   data: [49.9]

    // },
    // {
    //   name: 'B',
    //   data: [83.6]

    // },
    // {
    //   name: 'C',
    //   data: [48.9]

    // }, {
    //   name: 'BR',
    //   data: [42.4]

    // },
    // {
    //   name: 'SBR',
    //   data: [42.4]
    // }
  ]
});
</script>