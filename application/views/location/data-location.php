<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-map-marker"></i> Master Data Lokasi</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Lokasi</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role') == 'SUPERADMIN'){ ?>
                    <!-- <button class="btn btn-outline btn-info dim" id="btn_add_data">
                        <i class="fa fa-plus"></i> Add Lokasi
                    </button> -->
                    <button id="btn_add_data" class="btn btn-info " type="button"><i class="fa fa-plus"></i> Add Lokasi</button>
                </div> <?php } ?>
                <div class="ibox-content">
                    <div class="table-responsive">
                    <table id="tableLocation" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Kode Lokasi</th>
                        <th>Nama Lokasi</th>
                        <?php if($this->session->userdata('role') == 'SUPERADMIN'){?>
                        <th width="15%">Aksi</th>
                        <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-quis">Tambah Lokasi</h6>
            </div>
            <div class="modal-body">
                <form id="formAddlocation" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kode Lokasi</label>
                        <div class="col-sm-7">
                            <input type="hidden" class="form-control" name="id_location" id="id_location" required>
                            <input type="text" class="form-control" name="location_code" id="location_code" placeholder="Kode Lokasi" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Lokasi</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="location_name" id="location_name" placeholder="Nama Lokasi" required>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center;">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>;
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("document").ready(function(){
        var tableLocation = $("#tableLocation").dataTable({
            processing: true,
            select: false,
            ajax: {
                url : "<?= base_url('location/get_data_location') ?>",
                dataType : "JSON",
                type : "GET",
                dataSrc : function (data){
                    var returnDatalocation = new Array();
                    if(data.status == "success"){
                        $.each(data["data"], function(i, item){
                            returnDatalocation.push({
                                "no" : (i+1),
                                "location_code" : item["location_code"],
                                "location_name" : item["location_name"],
                                "action" : "<center><button id='btn_edit' data-id_location='"+item["id_location"]+"' class='btn btn-xs btn-warning'><i class='fa fa-edit'></i> Edit</button>&nbsp;"+
                                            "<button id='btn_delete' data-id_location='"+item["id_location"]+"' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Delete</button></center>"
                            });
                        });
                    }
                    return returnDatalocation;
                }
            },
            columns : [
                {data : "no"},
                {data : "location_code"},
                {data : "location_name"},
                {data : "action"}
            ]
        });
    });

    $(document).on("click", "#btn_add_data", function(e){
        $('#formAddlocation')[0].reset();
        $("#id_location").val("");
        $("#modalAdd").modal("show");
    });

    $(document).on("click", "#btn_edit", function(e){
        e.preventDefault();
        $('#formAddlocation')[0].reset();
        var id_location = $(this).data("id_location");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('location/get_data_location') ?>/"+id_location,
            "method": "GET",
        }).done(function (response) {
            var data = JSON.parse(response);
            $("#id_location").val(data.data[0].id_location);
            $("#location_code").val(data.data[0].location_code);
            $("#location_name").val(data.data[0].location_name);
            $("#modalAdd").modal("show");
        });
    });
    

    $(document).on("submit", "#formAddlocation", function(e){
        e.preventDefault();
        var id_location = $("#id_location").val();
        var url = (id_location == "" ? "addLocation" : "editLocation");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('location') ?>/"+url,
            "method": "POST",
            "data": $(this).serialize(),
        }).done(function (response) {
            var data = JSON.parse(response)
            var message = data.message;
            if(data.status == "success"){
                $("#modalAdd").modal("hide");
                swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#tableLocation").DataTable().ajax.reload();
                });
                
            } else {
                swal("Gagal menambahkan.", message.toUpperCase(), "warning");
            }
        });
    });

    $(document).on("click", "#btn_delete", function(e){
        var id_location = $(this).data("id_location");
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('location/deleteLocation') ?>",
                    "method": "POST",
                    "data": {
                        "id_location": id_location
                    }
                }
                
                $.ajax(settings).done(function (response) {
                    var data = JSON.parse(response)
                    var message = data.message;
                    if(data.status == "success"){
                        swal({
                            title: "Berhasil",
                            text: message.toUpperCase(),
                            type: "success",
                            confirmButtonColor: "#a5dc86",
                            confirmButtonText: "Close",
                        }, function(isConfirm){
                            $("#tableLocation").DataTable().ajax.reload();
                        });
                    } else {
                        swal("Gagal menghapus data.", message.toUpperCase(), "warning");
                    }
                });   
            }
        });
    });
</script>