<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-calendar"></i> Master Timesheet </h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('Dashboard'); ?>">Dashboard</a></li>
            <li><a  href="<?php echo base_url('Periode'); ?>">Periode</a></li>
            <li class="active"><strong><a>Timesheet</a></strong></li>
        </ol>
    </div>
</div>
<!--<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-5">
                <div class="form-group">
                    <label>Nama Periode</label>
                     <input type="text" class="form-control" value="<?php echo $kriteria->nama; ?>" readonly required=""> 
                </div>    
            </div>
        </div>
    </div>
</div>-->
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role')=='SUPERADMIN'){ ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-2">
                                <button class="btn btn-outline btn-info" data-toggle="modal" data-target="#myModal5">
                                    <i class="fa fa-plus"></i> Add Timesheet
                                </button>
                            </div>                       
                            
                        </div>
                    </div>
                    
                </div> <?php } ?>
                <div class="ibox-content">
                <?php if($this->session->flashdata("success")){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("success"));
                            unset($_SESSION["success"]);
                        ?>
                    </div>
                <?php } else if($this->session->flashdata("error")) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("error"));
                            unset($_SESSION["error"]);
                        ?>
                    </div>
                <?php } ?>
                    <div class="table-responsive">
                    <table id="tablesub" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th><center>Category</center></th>
                        <th><center>Cost Control</center></th>
                        <th><center>Description</center></th>
                        <th><center>Attachment</center></th>
                        <th><center>Client</center></th>
                        <th><center>Location</center></th>
                        <th><center>Keterangan</center></th>
                        <th><center>Activity</center></th>
                        <th><center>Start</center></th>
                        <th><center>End</center></th>
                        <?php if($this->session->userdata('role')=='SUPERADMIN'){?>
                        <th width="10%"><center>Action</center></th>
                        <?php } ?>
                    </tr>
                   </thead>
                   <!-- <tbody>
                    <?php $i=1; foreach ($sub_kriteria as $p) { ?>
                    <tr>
                        <td><center><?php echo $i; ?></center></td>
                        <td><center><?php echo $p->nm_kriteria; ?></center></td>
                        <td><center><?php echo $p->nama_sub; ?></center></td>
                        <td>
                            <center>
                                <a class="btn btn-info btn-xs"  data-toggle="modal" data-target="#modal-edit<?=$p->id_sub_kriteria;?>"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo site_url('kriteria/delete_sub/'.$p->id_sub_kriteria); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data <?=$p->nama_sub;?> ?');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                            </center>
                        </td>
                    </tr>
                    <?php  $i++; } ?>
                    </tbody> -->
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- modal baru -->
    <div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Tambah Time Sheet</h4>
                </div>
                <form class="form-horizontal" action="<?php echo base_url('periode/action_tambah_sub'); ?>" method="POST" role="form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Cost Control</label>
                                     <select class="form-control" id="idControl" name="id_cost_control" required>
                                    <option value="">Pilih Cost Control</option>
                                    <?php
                                        foreach ($control as $l) {
                                            echo '<option value="'.$l->id_cost_control.'">'.$l->cost_control_code.'</option>';
                                        }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Category</label>
                                    <input id="category_name" name="category" type="text" class="form-control" required="required">
                                    <input id="category_id" name="id_category" type="hidden" class="form-control" required="required">
                                </div>
                            </div> 
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" id="cost_description" rows="2" required=""></textarea>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Attachment</label>
                                    <input id="userName" name="userName" type="text" class="form-control" required="required">
                                </div>
                            </div> 
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Client</label>
                                    <input id="userName" name="userName" type="text" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Location</label>
                                    <input id="userName" name="userName" type="text" class="form-control" required="required">
                                </div>
                            </div> 
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Activity</label>
                                    <input id="userName" name="userName" type="text" class="form-control"  required="required">
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <input id="userName" name="userName" type="text" class="form-control" required="required">
                                </div>
                            </div> 
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Tanggal Start</label>
                                    <input id="userName" name="userName" type="text" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Tanggal End</label>
                                    <input id="userName" name="userName" type="text" class="form-control" required="required">
                                </div>
                            </div> 
                        </div>
                        
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
     $(document).ready(function() {
         $('#departmen').select2();
     });
     $(".select2_demo_1").select2();
     $(".select2_demo_2").select2();
     $(".select2_demo_3").select2({
        placeholder: "Select a state",
        allowClear: true
       });
    </script>
    <script>
        $(document).ready(function(){
        $("#tablesub").dataTable();
        $("#tablepenjualan1").dataTable();
    });
    </script>
    <script type="text/javascript">
        $(document).on("change", "#idControl", function(e){
            var idControl = $(this).val();
            $.ajax({
                url: "<?php echo base_url() ?>Periode/cost_control/"+idControl,
                type: "GET",
                success: function(data){
                    var obj = JSON.parse(data);
                    var control = obj.data;
                    var list_control = "";
                    $("#category_id").val(control[0].id_category);
                    $("#category_name").val(control[0].category_name);
                    $("#cost_control_id").val(control[0].cost_control_id);
                    $("#cost_description").val(control[0].cost_description);
                    // getSaldo();

                }
            })
        });
    </script>
