<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-life-bouy"></i> Master Data Periode</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Periode</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <?php if($this->session->userdata('role')=='SUPERADMIN'){ ?>
                    <button class="btn btn-info btn-info" data-toggle="modal" data-target="#ModalaAdd">
                        <i class="fa fa-plus"></i> Add Periode
                    </button>
                <?php } ?>
                </div>
                <div class="ibox-content">

                    <?php if($this->session->flashdata("success")){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("success"));
                            unset($_SESSION["success"]);
                        ?>
                    </div>
                <?php } else if($this->session->flashdata("error")) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("error"));
                            unset($_SESSION["error"]);
                        ?>
                    </div>
                <?php } ?>


                    <div class="row">
                        
                    </div>
                    <div class="table-responsive">
                    <table id="tablePeriode" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <?php if($this->session->userdata('role') == 'SUPERADMIN'){?>
                        <th width="15%"><center>Aksi</center></th>
                        <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                    <?php $i=1; foreach ($periode as $p) { ?>
                    <tr>
                        <td><?php echo $i?></td>
                        <td><?php echo $p->nm_user; ?></td>
                        <td><?php echo $p->date_start; ?></td>
                        <td><?php echo $p->date_end; ?></td>
                        <td><center>
                        <?php if($this->session->userdata('role') == 'SUPERADMIN'){ ?>
                            <a href="<?php echo base_url('periode/detail/'.$p->id_periode); ?>" class="btn btn-xs btn-info"><i class="fa fa-book"></i></a>
                            <a href="<?php echo base_url('periode/edit/'.$p->id_periode); ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                            <a href="<?php echo base_url('periode/delete/'.$p->id_periode); ?>" class="btn btn-xs btn-danger" onclick="return confirm ('Yakin ingin hapus')" ><i class="fa fa-trash"></i></a>
                            
                        <?php } ?></center>
                        </td>
                    
                    </tr>
                   
                   <?php  $i++; } ?>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModalaAdd" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Tambah Periode</h3>
            </div>
            <form class="form-horizontal" action="<?php echo base_url('periode/action_tambah'); ?>" method="POST" role="form">
                <div class="modal-body">

                    <div class="form-group">
                        <div class="col-sm-7">
                            <input type="hidden" class="form-control" name="id_user" value="<?php echo $this->session->userdata('id_user')?>" required="">
                        </div>
                    </div>

                    <div class="form-group" id="data_1">
                        <label class="control-label col-xs-3" ><center>Tanggal Start</center></label>
                        <div class="col-xs-7">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar">
                                </i></span>
                                    <input type="text" class="form-control" name="date_start" value="<?php echo date('d/m/Y', strtotime('previous sunday') ); ?>" required="" readonly>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-info" id="btn_simpan">Simpan</button>
                </div>
            </form>
            </div>
            </div>
    </div>
     <script>
        // swal("Good job!", "You clicked the button!", "success")
    </script>

    <script>
        $(document).ready(function(){
        $("#tablePeriode").dataTable();
    });
    </script>
    <script>
    $(document).ready(function(){
    $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            endDate: new Date(new Date().setDate(new Date().getDate() + 0)),
            daysOfWeekDisabled: [1,2,3,4,5,6],
            format:'dd/mm/yyyy',
            firstDay: 1 // Start with Monday



        }); 
    $("#inputmasking1, #inputmasking2, #inputmasking3, #inputmasking4, #inputmasking5, #inputmasking6, #inputmasking7, #inputmasking8").mask("000.000.000.000", {reverse: true});
    });

    $("#percentmasking").mask("##0,00", {reverse: true});
    </script>

    <script>
        $(document).on("submit", "#formAddactivity", function(e){
        e.preventDefault();
        var id_activity = $("#id_activity").val();
        var url = (id_activity == "" ? "addActivity" : "editActivity");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('activity') ?>/"+url,
            "method": "POST",
            "data": $(this).serialize(),
        }).done(function (response) {
            var data = JSON.parse(response)
            var message = data.message;
            if(data.status == "success"){
                $("#modalAdd").modal("hide");
                swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#tableActivity").DataTable().ajax.reload();
                });
                
            } else {
                swal("Gagal menambahkan.", message.toUpperCase(), "warning");
            }
        });
    });
    </script>

    <script>
        $(document).ready(function () {

        $('.demo1').click(function(){
            swal({
                title: "Welcome in Alerts",
                text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
            });

        });

        $('.demo2').click(function(){
            swal({
                title: "Good job!",
                text: "You clicked the button!",
                type: "success"
            });
        });

    });

    </script>

