<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-user"></i> Master Data Timesheet</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Timesheet</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role') == 'SUPERADMIN'){ ?>
                    <!-- <button class="btn btn-outline btn-info dim" id="btn_add_data">
                        <i class="fa fa-plus"></i> Add Klien
                    </button> -->
                    <button id="btn_add_data" class="btn btn-info " type="button"><i class="fa fa-plus"></i> Add Timesheet</button>
                </div> <?php } ?>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-1 pull-right" style="margin-right:40px;">
                            <label><br/></label>
                                <a href="<?php echo base_url('pertanyaan/cetakPdf') ?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak Data</a>
                        </div>
                        <div class="col-lg-1 pull-right" style="margin-right:10px;">
                            <label><br/></label>
                                <a href="<?php echo base_url('pertanyaan/export') ?>" data-toggle="tooltip" title="Download" class="btn btn-sm btn-primary"><i class="fa fa-file-excel-o"></i> Ekspor</a>
                        </div>
                        
                    </div>
                    <div class="table-responsive">
                    <table id="tableTimesheet" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Nama User</th>
                        <th>Cost Control</th>
                        <th>Description</th>
                        <th>Attachment</th>
                        <th>Client</th>
                        <th>Location</th>
                        <th>Information</th>
                        <th>Activity</th>
                        <th>Start</th>
                        <th>End</th>
                        <?php if($this->session->userdata('role') == 'SUPERADMIN'){?>
                        <th width="15%">Action</th>
                        <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-quis">Choose Periode</h6>
            </div>
            <div class="modal-body">
                <form id="formAddclient" class="form-horizontal">
                    <div class="form-group" id="data_1">
                        <label class="col-sm-3 control-label">Start</label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar">
                                    </i></span>
                                    <div class="col-sm-7">
                                    <input type="text" id="tgl_mulai" class="form-control" name="tgl_mulai" value="<?php echo date('d/m/Y')?>" required="">
                                </div>
                            </div>
                    </div>
                    <div class="form-group" id="data_1">
                        <label class="col-sm-3 control-label">End</label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar">
                                    </i></span>
                                    <div class="col-sm-7">
                                    <input type="text" id="tgl_mulai" class="form-control" name="tgl_mulai" value="<?php echo date('d/m/Y')?>" required="">
                                </div>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>&emsp;&emsp;&emsp;&emsp;&emsp;
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
        var tableTimesheet = $("#tableTimesheet").dataTable({
            processing: true,
            select: false,
            ajax: {
                url : "<?= base_url('timesheet/get_data_timesheet') ?>",
                dataType : "JSON",
                type : "GET",
                dataSrc : function (data){
                    var returndataTimesheet = new Array();
                    if(data.status == "success"){
                        $.each(data["data"], function(i, item){
                            returndataTimesheet.push({
                                "no" : (i+1),
                                "usename" : item["usename"],
                                "codecost" : item["codecost"],
                                "descriptcost" : item["descriptcost"],
                                "attachment" : item["attachment"],
                                "codecli" : item["codecli"],
                                "codeloc" : item["codeloc"],
                                "timesheet_information" : item["timesheet_information"],
                                "codeact" : item["codeact"],
                                "start_time" : item["start_time"],
                                "end_time" : item["end_time"],
                                "action" : "<center><button id='btn_edit' data-id_timesheet='"+item["id_timesheet"]+"' class='btn btn-xs btn-warning'><i class='fa fa-edit'></i> Ubah</button>&nbsp;"+
                                            "<button id='btn_delete' data-id_timesheet='"+item["id_timesheet"]+"' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Hapus</button></center>"
                            });
                        });
                    }
                    return returndataTimesheet;
                }
            },
            columns : [
                {data : "no"},
                {data : "usename"},
                {data : "codecost"},
                {data : "descriptcost"},
                {data : "attachment"},
                {data : "codecli"},
                {data : "codeloc"},
                {data : "timesheet_information"},
                {data : "codeact"},
                {data : "start_time"},
                {data : "end_time"},
                {data : "action"}
            ]
        });
    });

    $(document).on("click", "#btn_add_data", function(e){
        $('#formAddclient')[0].reset();
        $("#id_timesheet").val("");
        $("#modalAdd").modal("show");
    });

    $(document).on("click", "#btn_edit", function(e){
        e.preventDefault();
        $('#formAddclient')[0].reset();
        var id_timesheet = $(this).data("id_timesheet");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('client/get_data_client') ?>/"+id_timesheet,
            "method": "GET",
        }).done(function (response) {
            var data = JSON.parse(response);
            $("#id_timesheet").val(data.data[0].id_timesheet);
            $("#client_code").val(data.data[0].client_code);
            $("#client_name").val(data.data[0].client_name);
            $("#modalAdd").modal("show");
        });
    });
    

    $(document).on("submit", "#formAddclient", function(e){
        e.preventDefault();
        var id_timesheet = $("#id_timesheet").val();
        var url = (id_timesheet == "" ? "addClient" : "editClient");
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('client') ?>/"+url,
            "method": "POST",
            "data": $(this).serialize(),
        }).done(function (response) {
            var data = JSON.parse(response)
            var message = data.message;
            if(data.status == "success"){
                $("#modalAdd").modal("hide");
                swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#tableTimesheet").DataTable().ajax.reload();
                });
                
            } else {
                swal("Gagal menambahkan.", message.toUpperCase(), "warning");
            }
        });
    });

    $(document).on("click", "#btn_delete", function(e){
        var id_timesheet = $(this).data("id_timesheet");
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('client/deleteClient') ?>",
                    "method": "POST",
                    "data": {
                        "id_timesheet": id_timesheet
                    }
                }
                
                $.ajax(settings).done(function (response) {
                    var data = JSON.parse(response)
                    var message = data.message;
                    if(data.status == "success"){
                        swal({
                            title: "Berhasil",
                            text: message.toUpperCase(),
                            type: "success",
                            confirmButtonColor: "#a5dc86",
                            confirmButtonText: "Close",
                        }, function(isConfirm){
                            $("#tableTimesheet").DataTable().ajax.reload();
                        });
                    } else {
                        swal("Gagal menghapus data.", message.toUpperCase(), "warning");
                    }
                });   
            }
        });
    });
</script>
<script>
    $(document).ready(function(){
    $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format:'dd/mm/yyyy'

        });
});
</script>