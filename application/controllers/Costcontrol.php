<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Costcontrol extends CI_Controller {

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        // $this->load->library('pdf');
        $this->load->model("Model_cost");
    }

    public function index() {
    	$data['cc'] = $this->Model_cost->getCost();
        $data['listcategori'] = $this->Model_cost->category_list();
        $data['listuser'] = $this->Model_cost->user_list();
    	$this->template->load("template", "costcontrol/data-costcontrol", $data);
    }

    public function get_data_cc($idcc = null){
        $data = $this->Model_cost->getCost($idcc);
        echo json_encode(array("status" => "success", "data" => $data));
    }

    public function addCost(){
        $id_category = $this->input->post("id_category");
        $id_user = $this->input->post("id_user");
        $cost_control_code = $this->input->post("cost_control_code");
        $cost_description = $this->input->post("cost_description");

        $dataCost = array(
            "id_category" => $id_category,
            "id_user" => $id_user,
            "cost_control_code" => $cost_control_code,
            "cost_description" => $cost_description
        );

        $same_code = $this->Model_cost->check_existing_code("cost_control_code", $cost_control_code);
        if($same_code){
            echo json_encode(array("status" => "error", "message" => "Kode Cost sudah ada, silahkan masukkan kode Cost lain"));
        } else {
            $addCost = $this->Model_cost->add_cost($dataCost);
            if($addCost){
                echo json_encode(array("status" => "success", "message" => "Berhasil menambahkan Cost", "data" => $dataCost));
            } else {
                echo json_encode(array("status" => "error", "message" => "Gagal menambahkan Cost"));
            }
        }
        

    }

    public function editCost(){
        $id_cost_control = $this->input->post("id_cost_control");
        $id_category = $this->input->post("id_category");
        $id_user = $this->input->post("id_user");
        $cost_control_code = $this->input->post("cost_control_code");
        $cost_description = $this->input->post("cost_description");

        $data = array(
            "id_category" => $id_category,
            "id_user" => $id_user,
            "cost_control_code" => $cost_control_code,
            "cost_description" => $cost_description
        );

        $editCost = $this->Model_cost->edit_cost($data, $id_cost_control);
        if($editCost){
            echo json_encode(array("status" => "success", "message" => "Data berhasil diubah", "data" => $data));
        } else {
            echo json_encode(array("status" => "error", "message" => "Data gagal diubah"));
        }
    }

    public function deleteCost(){
        $id_cost_control = $this->input->post("id_cost_control");
        $delete = $this->Model_cost->delete_cost($id_cost_control);
        if($delete){
            echo json_encode(array("status" => "success", "data" => $id_cost_control, "message" => "Data berhasil dihapus"));
        } else {
            echo json_encode(array("status" => "error", "message" => "Data gagal dihapus"));
        }
    }
}