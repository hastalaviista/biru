<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends CI_Controller {

    public function __construct(){
        parent::__construct();
        checkSessionUser();
        // $this->load->library('pdf');
        $this->load->model("Model_periode");
    }

    public function index() {
        // $data["control"]          = $this->Model_master->getMasterOneTable("*", "tbl_cost_control", "id_cost_control", "asc", "");
        $data["control"]      = $this->Model_periode->get_cost_control();
        $data["periode"]      = $this->Model_periode->get_periode();
        // $data['activity'] = $this->Model_activity->getActivity();
        $this->template->load("template", "periode/data-periode", $data);
        // $this->template->load("template", "periode/data-periode", $data);
    }

    public function timesheet($id){
        $data["control"]      = $this->Model_periode->get_cost_control();
        $data['periode']      = $this->db->get_where('tbl_periode',['id_periode' => $id])->row();
        // $this->template->load("template", "periode/sub-periode", $data);
        $this->template->load("template", "periode/time-sheet", $data);
        // print_r($data);
    }

    public function cost_control($idControl){
        $control = $this->Model_periode->view_cost_control($idControl);
        echo json_encode(array("status" => "success", "data" => $control));
      }

    public function action_tambah(){
        $id_user            = $this->input->post("id_user");
        $date_start         = $this->input->post("date_start");
        
        
        $start = date("Y-m-d", strtotime(str_replace("/", "-", $date_start)));

        $tanggal_exist      = $this->Model_periode->check_tanggal($start);

        // print_r($tanggal_exist);

        if ($tanggal_exist) {
            // echo "tidak boleh";
            $this->session->set_flashdata("error", "GAGAL MENYIMPAN KARENA TANGGAL SUDAH ADA");
            // echo "swal('Good job!', 'You clicked the button!', 'success')";
            // echo json_encode(array("status" => "error", "message" => "GAGAL MENYIMPAN KARENA TANGGAL SUDAH ADA"));


            redirect("periode");
        } else {
            // echo "boleh";
            $dataPeriode = array(
                "id_user"       => $id_user,
                "date_start"    => date("Y-m-d", strtotime(str_replace("/", "-", $date_start))),
                "date_end"      => date('Y-m-d', strtotime('+6 days', strtotime($start)))
            );

            $query = $this->Model_periode->tambahMaster("tbl_periode", $dataPeriode);

            if($query){
                $this->session->set_flashdata("success", "BERHASIL MENYIMPAN");
            } else {
                $this->session->set_flashdata("error", "GAGAL MENYIMPAN");
            }
            $id = $this->db->insert_id();

            redirect("periode/timesheet/$id");
        }
    }

    public function delete($id_periode){
            $_id = $this->db->get_where('tbl_periode',['id_periode' => $id_periode])->row();
            $query = $this->db->delete('tbl_periode',['id_periode'=>$id_periode]);
            if($query){
                $this->session->set_flashdata("success", "BERHASIL MENGHAPUS DATA");
            } else {
                $this->session->set_flashdata("error", "GAGAL MENGHAPUS DATA");
            }
            redirect('periode');
    }




}