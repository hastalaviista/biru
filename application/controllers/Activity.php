<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        // $this->load->library('pdf');
        $this->load->model("Model_activity");
    }

    public function index() {
    	$data['activity'] = $this->Model_activity->getActivity();
    	$this->template->load("template", "activity/data-activity", $data);
    }

    public function get_data_activity($idactivity = null){
        $data = $this->Model_activity->getActivity($idactivity);
        echo json_encode(array("status" => "success", "data" => $data));
    }

    public function addActivity(){
    	$activity_code = $this->input->post("activity_code");
    	$activity_name = $this->input->post("activity_name");

    	$dataActivity = array(
    		"activity_code" => $activity_code,
    		"activity_name" => $activity_name
    	);

        $same_code = $this->Model_activity->check_existing_code("activity_code", $activity_code);
        if($same_code){
            echo json_encode(array("status" => "error", "message" => "Code activity already exist, please use anothe code"));
        } else {
            $addActivity = $this->Model_activity->add_activity($dataActivity);
                if($addActivity){
                    echo json_encode(array("status" => "success", "message" => "Berhasil menambahkan aktifitas", "data" => $dataActivity));
                } else {
                    echo json_encode(array("status" => "error", "message" => "Gagal menambahkan aktifitas"));
                }
        }
    	

    }

    public function editActivity(){
    	$id_activity = $this->input->post("id_activity");
    	$activity_code = $this->input->post("activity_code");
    	$activity_name = $this->input->post("activity_name");

    	$data = array(
    		"activity_code" => $activity_code,
    		"activity_name" => $activity_name
    	);

    	$editActivity = $this->Model_activity->edit_activity($data, $id_activity);
    	if($editActivity) {
    		echo json_encode(array("status" => "success", "message" => "Data berhasil diubah", "data" => $data));
    	} else {
    		echo json_encode(array("status" => "error", "message" => "Data gagal diubah"));
    	}
    }

    public function deleteActivity(){
    	$id_activity = $this->input->post("id_activity");
    	$delete = $this->Model_activity->delete_activity($id_activity);
    	if($delete){
    		echo json_encode(array("status" => "success", "data" => $id_activity, "message" => "Data berhasil dihapus"));
    	} else {
    		echo json_encode(array("status" => "error", "message" => "Data gagal dihapus"));
    	}
    }




}