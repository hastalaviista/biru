<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        // $this->load->library('pdf');
        $this->load->model("Model_client");
    }

    public function index() {
    	$data['client'] = $this->Model_client->getClient();
    	$this->template->load("template", "client/data-client", $data);
    }

    public function get_data_client($idclient = null){
        $data = $this->Model_client->getClient($idclient);
        echo json_encode(array("status" => "success", "data" => $data));
    }

    public function addClient(){
        $client_code = $this->input->post("client_code");
        $client_name = $this->input->post("client_name");

        $dataClient = array(
            "client_code" => $client_code,
            "client_name" => $client_name
        );

        $addClient = $this->Model_client->add_client($dataClient);
        if($addClient){
            echo json_encode(array("status" => "success", "message" => "Berhasil menambahkan aktifitas", "data" => $dataClient));
        } else {
            echo json_encode(array("status" => "error", "message" => "Gagal menambahkan aktifitas"));
        }

    }

    public function editClient(){
        $id_client = $this->input->post("id_client");
        $client_code = $this->input->post("client_code");
        $client_name = $this->input->post("client_name");

        $data = array(
            "client_code" => $client_code,
            "client_name" => $client_name
        );

        $editClient = $this->Model_client->edit_client($data, $id_client);
        if($editClient) {
            echo json_encode(array("status" => "success", "message" => "Data berhasil diubah", "data" => $data));
        } else {
            echo json_encode(array("status" => "error", "message" => "Data gagal diubah"));
        }
    }

    public function deleteClient(){
        $id_client = $this->input->post("id_client");
        $delete = $this->Model_client->delete_client($id_client);
        if($delete){
            echo json_encode(array("status" => "success", "data" => $id_client, "message" => "Data berhasil dihapus"));
        } else {
            echo json_encode(array("status" => "error", "message" => "Data gagal dihapus"));
        }
    }
}