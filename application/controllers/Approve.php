<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approve extends CI_Controller {

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        // $this->load->library('pdf');
        $this->load->model("Model_approve");
    }

    public function index() {
    	$data['approve'] = $this->Model_approve->getApprove();
    	$this->template->load("template", "approve/data-approve", $data);
    }

    public function get_data_approve($idlocation = null){
        $data = $this->Model_approve->getApprove($idlocation);
        echo json_encode(array("status" => "success", "data" => $data));
    }
}