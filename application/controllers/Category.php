<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        // $this->load->library('pdf');
        $this->load->model("Model_category");
    }

    public function index(){
    	$data['category'] = $this->Model_category->getCategory();
    	$this->template->load("template", "category/data-category");
    }

    public function get_data_category($idcategory = null){
        $data = $this->Model_category->getCategory($idcategory);
        echo json_encode(array("status" => "success", "data" => $data));
    }

    public function addCategory(){
        $category_code = $this->input->post("category_code");
        $category_name = $this->input->post("category_name");
        $category_description = $this->input->post("category_description");

        $dataCategory = array(
            "category_code" => $category_code,
            "category_name" => $category_name,
            "category_description" => $category_description
        );

        $same_code = $this->Model_category->check_existing_code("category_code", $category_code);
        if($same_code) {
            echo json_encode(array("status" => "error", "message" => "Kode kategori sudah ada, silahkan masukkan kode kategori lain"));
        } else {
           $addCategory = $this->Model_category->add_category($dataCategory);
           if($addCategory){
            echo json_encode(array("status" => "success", "message" => "Berhasil menambahkan aktifitas", "data" => $dataCategory));
            } else {
                echo json_encode(array("status" => "error", "message" => "Gagal menambahkan aktifitas"));
            } 
        }
        

    }

    public function editCategory(){
        $id_category = $this->input->post("id_category");
        $category_code = $this->input->post("category_code");
        $category_name = $this->input->post("category_name");
        $category_description = $this->input->post("category_description");

        $data = array(
            "category_code" => $category_code,
            "category_name" => $category_name,
            "category_description" => $category_description
        );

        $editCategory = $this->Model_category->edit_category($data, $id_category);
        if($editCategory) {
            echo json_encode(array("status" => "success", "message" => "Data berhasil diubah", "data" => $data));
        } else {
            echo json_encode(array("status" => "error", "message" => "Data gagal diubah"));
        }
    }

    public function deleteCategory(){
        $id_category = $this->input->post("id_category");
        $delete = $this->Model_category->delete_category($id_category);
        if($delete){
            echo json_encode(array("status" => "success", "data" => $id_category, "message" => "Data berhasil dihapus"));
        } else {
            echo json_encode(array("status" => "error", "message" => "Data gagal dihapus"));
        }
    }

}