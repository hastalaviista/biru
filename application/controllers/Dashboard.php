<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
        checkSessionUser();
		$this->load->model("Model_dashboard");
	}

	public function index(){
        $data["user"]        = $this->Model_dashboard->view_user();
        $data["wappr"]       = $this->Model_dashboard->view_wappr();
        $data["apprpe"]      = $this->Model_dashboard->view_apprpe();
        $data["reject"]      = $this->Model_dashboard->view_reject();
        // print_r($data);
		$this->template->load("template", "dashboard/v-dashboard", $data);
	}
}