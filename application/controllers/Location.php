<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller {

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        // $this->load->library('pdf');
        $this->load->model("Model_location");
    }

    public function index() {
    	$data['location'] = $this->Model_location->getLocation();
    	$this->template->load("template", "location/data-location", $data);
    }

    public function get_data_location($idlocation = null){
        $data = $this->Model_location->getLocation($idlocation);
        echo json_encode(array("status" => "success", "data" => $data));
    }

    public function addLocation(){
        $location_code = $this->input->post("location_code");
        $location_name = $this->input->post("location_name");

        $dataLocation = array(
            "location_code" => $location_code,
            "location_name" => $location_name
        );

        $same_code = $this->Model_location->check_existing_code("location_code", $location_code);
        if($same_code){
            echo json_encode(array("status" => "error", "message" => "Kode lokasi sudah ada, silahkan masukkan kode lokasi lain"));
        } else {
            $addLocation = $this->Model_location->add_location($dataLocation);
            if($addLocation){
                echo json_encode(array("status" => "success", "message" => "Berhasil menambahkan aktifitas", "data" => $dataLocation));
            } else {
                echo json_encode(array("status" => "error", "message" => "Gagal menambahkan aktifitas"));
            }
        }
        
    }

    public function editLocation(){
        $id_location = $this->input->post("id_location");
        $location_code = $this->input->post("location_code");
        $location_name = $this->input->post("location_name");

        $data = array(
            "location_code" => $location_code,
            "location_name" => $location_name
        );

        $editLocation = $this->Model_location->edit_location($data, $id_location);
        if($editLocation) {
            echo json_encode(array("status" => "success", "message" => "Data berhasil diubah", "data" => $data));
        } else {
            echo json_encode(array("status" => "error", "message" => "Data gagal diubah"));
        }
    }

    public function deleteLocation(){
        $id_location = $this->input->post("id_location");
        $delete = $this->Model_location->delete_location($id_location);
        if($delete){
            echo json_encode(array("status" => "success", "data" => $id_location, "message" => "Data berhasil dihapus"));
        } else {
            echo json_encode(array("status" => "error", "message" => "Data gagal dihapus"));
        }
    }
}