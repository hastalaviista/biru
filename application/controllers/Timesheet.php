<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timesheet extends CI_Controller {

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        // $this->load->library('pdf');
        $this->load->model("Model_timesheet");
    }

    public function index() {
    	$data['timesheet'] = $this->Model_timesheet->getTimesheet();
    	$this->template->load("template", "timesheet/data-timesheet", $data);
    }

    public function get_data_timesheet($idtimesheet = null){
        $data = $this->Model_timesheet->getTimesheet($idtimesheet);
        echo json_encode(array("status" => "success", "data" => $data));
    }
}
